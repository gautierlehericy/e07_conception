<?php
include 'navbar.php';
verifyconnect();
$film = getfilm($_GET['episode']);
if(isset($_POST['supprimerfilm'])){
  $deletefilm = $bdd->prepare("DELETE FROM Film WHERE id=?");
  $deletefilm->execute(array($film['id']));
}
?>
<div class="container bg-light">
  <div class="row">
    <div class="col text-center">
      <h3><?php echo $film['titre'];?></h3><br>
      <?php
        if(verifyadmin()==1){?>
          <a href="modifierfilm.php?episode=<?php echo $film['episode']?>">Clique ici si tu veux modifier cette page ;)</a>
           <form method="POST">
            <button type="submit" name="supprimerfilm" class="btn btn-primary">Supprimer le film</button>
          </form><?php  
        }?>
       

      

    </div>
  </div><hr>
  <!-- info du film -->
  <div class="row">
    <div class="col-md-9 text-justify">
      <h4>Description de <?php echo $film['titre'];?>:</h4>
      <strong>Réalisateur : </strong><?php echo $film['réalisation'];?>.<br><br>
      <strong>Scénario : </strong><?php echo $film['scenario'];?>.<br><br>
      <strong>Durée du film : </strong><?php echo $film['duree'];?>.<br><br>
      <strong>Date de sortie du film : </strong><?php $datesortie = explode("-", $film['sortie']);afficherdate($datesortie);?>.<br><br>
      <strong>Synopsis : </strong><br><?php echo $film['synopsis'];?>
    </div>

    <!-- Photo + système notation --> 
    <div class="col">
      <img src="<?php echo $film['picture'];?>" width="100%" alt="<?php echo $film['titre'];?>">
      <link rel="stylesheet" href="CSS/rate.css"/>
      <?php $req = $bdd->prepare('SELECT AVG(note) FROM stars WHERE film = ? ');
      $req->execute(array($_GET['episode']));
      $moy = $req->fetch();
      echo "La moyenne des notes est de : ". number_format((float)$moy['AVG(note)'], 2, '.', '');
      
      // Vérifie si l'utilisateur est connecté pour pouvoir voter 
      if(isset($_COOKIE['idSession'])){
        if(verifyvote($_COOKIE['idSession'], $_GET['episode']) != 0){
          $vote = getvote($_COOKIE['idSession'], $_GET['episode']);
          echo "<br>Votre note est de : " . $vote['note'] . "<br> Vous pouvez renvoyer une nouvelle note en sélectionnant une note et en cliquant sur le bouton.";
        }
      }
      // Insère la note dans la BDD ou remplace la note 
      if(isset($_POST['rating'])){
        if(isset($_COOKIE['idSession'])){
          if(verifyvote($_COOKIE['idSession'], $_GET['episode']) == 0){
            $req = $bdd->prepare('INSERT INTO stars(note,film, userid) VALUES (?,?,?)');
            $req->execute(array($_POST['rating'],$_GET['episode'],$_COOKIE['idSession']
            ));
          } else {
            $voteupdate = $bdd->prepare("UPDATE stars SET note = ? WHERE userid = ? AND film = ?");
            $voteupdate->execute(array($_POST['rating'], $_COOKIE['idSession'], $_GET['episode']));
          }
        } else {
          $msgcon = "Erreur, veuillez vous connecter pour pouvoir voter";
          $opencon = 1;
        }
      }
      ?>

      <!-- votes --> 
      <form class="rating" method="POST">
        <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 étoile"></label>
        <input type="radio" id="star4half" name="rating" value="4,5" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
        <input type="radio" id="star3half" name="rating" value="3,5" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
        <input type="radio" id="star2half" name="rating" value="2,5" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
        <input type="radio" id="star1half" name="rating" value="1,5" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
        <input type="radio" id="starhalf" name="rating" value="0,5" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
        <p><input type="submit" /></p>     
      </form>      
    </div>
  </div>
  <hr>
  <!--- Bande annonce --->
  <div class="row">
    <div class="col text-center">
      <h1 style="text-align: center;">Bande-annonce</h1>
        <?php $lien = $bdd->prepare('SELECT bandeannonce FROM Film WHERE episode = ?');
          $lien->execute(array($_GET['episode']));
          $lienfilm = $lien->fetch();
          echo '<iframe src="' . $lienfilm['bandeannonce'] . '"height="720px"width="100%" frameborder="0" allowfullscreen=""></iframe>';?>
    </div>
  </div>
  <hr>
  
  <div class="row">
    <div class="col text-center">
      <h1>Liste des personnages :</h1>  
    </div>
  </div>
  <div class="row text-center">
    <?php
		  $con = connectionbdd();
		  $reqlistperso = $con->prepare("SELECT * FROM Film INNER JOIN PlaysIn ON Film.episode = PlaysIn.id_film INNER JOIN People ON PlaysIn.id_people = People.id WHERE id_film = ? ");
			$reqlistperso->execute(array($_GET['episode']));
			if($reqlistperso->rowCount() != 0){
        while($listperso = $reqlistperso->fetch()){ ?>
					<div class="col-md-4">
            <a href="pagepersonnage.php?nom=<?php echo $listperso['nom'];?>"><img src="<?php echo $listperso['picture']?>" style="border-radius: 50%;" width="130px " alt=""></a>
            <p><?php echo $listperso['nom'];?></p>
					</div><?php
				}
			} else { ?>
				<div class="col text-center">
					<h4>Désolé cette planète n'apparait dans aucun film.</h4>
				</div><?php
			}?>
  </div>

</div>


<?php
include 'footer.php';
?>