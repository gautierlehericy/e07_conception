<?php
include 'navbar.php';
verifyconnect();
    if(verifyadmin() == 0){?>
        <script>
            window.location.replace("index.php?con=1");
        </script><?php
    }
$film = getfilm($_GET['episode']);

if(isset($_POST['formmodifierfilm'])){
    $titre1 = htmlspecialchars($_POST['titre1']);
    $réalisation1 = htmlspecialchars($_POST['réalisation1']);
    $scenario1 = htmlspecialchars(($_POST['scenario1']));
    $duree1 = htmlspecialchars($_POST['duree1']);
    $episode1 = htmlspecialchars($_POST['episode1']);
    $picture1 = htmlspecialchars($_POST['picture1']);
    $synopsis1 = htmlspecialchars($_POST['synopsis1']);
    $trailer1 = htmlspecialchars($_POST['trailer1']);

    if(!empty($_POST['titre1']) AND !empty($_POST['réalisation1'])AND !empty($_POST['scenario1'])AND !empty($_POST['duree1'])
    AND !empty($_POST['episode1'])AND !empty($_POST['picture1'])AND !empty($_POST['synopsis1'])AND !empty($_POST['trailer1'])){
        $addperso = $bdd->prepare("UPDATE Film SET titre = ?, réalisation = ?, scenario = ? ,duree=?, episode=?, picture=?, synopsis=?, bandeannonce=? WHERE id=? ");
        $addperso->execute(array($titre1, $réalisation1, $scenario1,$duree1,$episode1,$picture1,$synopsis1,$trailer1,$film['id']));
        

    } else {
        $erreurajout = "Veuillez remplir tous les champs puis reessayer.";
        
    }
}

?>

<div class="container bg-light">
  <div class="row">
    <div class="col text-center">
      <h3><?php echo $film['titre'];?></h3>
    </div>
  </div><hr>
  <!-- info du film -->
  <div class="row">
    <div class="col-md-9 text-justify">
        <form action="" method="post">
            <div class="form-group">
            	<label for="titre">Titre :</label>
                <input type="title" class="form-control" placeholder="" name="titre1" id="titre1" value="<?php 
                echo $film['titre'];
                ?>">
            </div>
            		<div class="form-group">
            			<label for="réalisation">Réalisation :</label>
                        <input type="realization" class="form-control" placeholder="" name="réalisation1" id="réalisation1" value="<?php
                        echo $film['réalisation'];
                        ?>">
            		</div>
            		<div class="form-group">
            			<label for="scenario">Scénario :</label>
                        <input type="scenario" class="form-control" placeholder="" name="scenario1" id="scenario1" value="<?php
                        echo $film['scenario'];
                        ?>"> 
            		</div>
            		<div class="form-group">
            			<label for="duréefilm">Durée du film :</label>
                        <input type="duration" class="form-control" placeholder=""  name ="duree1" id="duree1" value ="<?php
                        echo $film['duree'];
                        ?>">
            		</div>
                    <div class="form-group">
            			<label for="episode">Episode :</label>
                        <input type="episode" class="form-control" placeholder="" name ="episode1" id="episode1" value ="<?php
                        echo $film['episode'];
                        ?>">
                    </div>
                    <div class="form-group">
            			<label for="picture">Photo :</label>
                        <input type="picture" class="form-control" placeholder="" name ="picture1" id="picture1" value ="<?php
                        echo $film['picture'];
                        ?>">
                    </div>
                    <div class="form-group">
            			<label for="synopsis">Synopsis :</label>
                        <input type="synopsis" class="form-control" placeholder="" name ="synopsis1" id="synopsis1" value ="<?php
                        echo $film['synopsis'];
                        ?>">
            		</div>
                    <div class="form-group">
        <label for="trailer">Bande annonnce :</label>
            <input type="trailer" class="form-control" placeholder="" name ="trailer1" id="trailer1" value ="<?php
            echo $film['bandeannonce'];
        ?>">
      </div>

            		<button type="submit" name="formmodifierfilm" class="btn btn-primary">Envoyer</button>
            				
            	</form>
    </div>

    <!-- Photo + système notation --> 
    <div class="col">
      <img src="<?php echo $film['picture'];?>" width="100%" alt="<?php echo $film['titre'];?>">
    </div>
  </div>
  <hr>
  <!--- Bande annonce --->
  

</div>


<?php
include 'footer.php';
?>