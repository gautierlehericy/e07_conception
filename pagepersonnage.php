<?php 
	include 'navbar.php';
	  verifyconnect();
	  $people = getpeople($_GET['nom']);
	  if(isset($_POST['supprimerperso'])){
		$deleteperso = $bdd->prepare("DELETE FROM People WHERE id=?");
		$deleteperso->execute(array($people['id']));
	}
?>
<!-- COMMENCEZ VOS BODY ICI -->
<div class="container">
	<div class="row">
        <div class="col-md-8">
        	<div id="wrappercentral" class="row">
        		<div class="col text-center">
        			<h2>Histoire du personnage : </h2>
        		</div>
        	</div>
        	<div id="wrappercentral"class="row">
        		<div class="col text-justify">
        			<?php 
        				echo nl2br($people['description']);
        			?>

        		</div>
        	</div>
        </div>
        <div class ="col-md-4" style ="background: #dbd9d9;">
			<img src="<?php 
				$image = $bdd->prepare("SELECT picture FROM People WHERE nom = ?");
				$image->execute(array($_GET['nom']));
				$picture = $image->fetch();
				echo $picture['picture'];
			?>
			" class="pt-3 img-fluid" style= "width: 350px; no-repeat center center" alt="Circle image" class="img-circle"/>
			<h3>
				<?php
					echo $_GET['nom'];
				?>
			</h3>
			<hr>
			<h style="font-size: 17px; margin-left: 150px;"><strong>Films </strong> </h><br>
			<?php 
				$liste = $bdd->prepare("SELECT titre, episode FROM Film INNER JOIN PlaysIn ON Film.episode = PlaysIn.id_film INNER JOIN People ON PlaysIn.id_people = People.id WHERE nom = ?");
				$lien = $bdd->prepare("SELECT episode FROM FILM INNER JOIN PlaysIn ON Film.episode = PlaysIn.id_film	INNER JOIN People ON PlaysIn.id_people = People.id WHERE nom =?");
				
				$liste->execute(array($_GET['nom']));
				$lien->execute(array($_GET['nom']));
				while ($listefilm = $liste->fetch()){?>
					<a href="film.php?episode=<?php echo $listefilm['episode'];?>" style="color: #730505; margin-left: 10px;"><?php echo $listefilm['titre'];?></a><br><?php
				}
			?>
			<hr>
			<h style="font-size: 17px; margin-left: 150px;"><strong>Affiliations</strong> </h><br>
			<?php
				$affiliation = $bdd->prepare("SELECT affiliations FROM People WHERE nom = ?");
				$affiliation->execute(array($_GET['nom']));
				while ($listeaffiliation = $affiliation->fetch()){
					echo '<p style= margin-left: 10px;">' . nl2br($listeaffiliation['affiliations']) . '</p>';
				}
			?>
			<hr>
			
			<?php
				$mondenatal = $bdd->prepare("SELECT name FROM Planet INNER JOIN People ON Planet.id = People.homeworld WHERE nom = ?");
				$mondenatal->execute(array($_GET['nom']));
				while ($listemondenatal = $mondenatal->fetch()){
					echo '<h style="font-size: 17px;"><strong>Locations : </strong>' . nl2br($listemondenatal['name']) . '</h>';
				}
			?>
			<hr>
			
				
				<?php 

				$reponse = $bdd->prepare('SELECT sexe FROM People WHERE nom = ?');
				$reponse->execute(array($_GET['nom']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Sexe : </strong> </h>' . '      ' . $donnees['sexe'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT taille FROM People WHERE nom = ?');
				$reponse->execute(array($_GET['nom']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Taille : </strong> </h>' . '      ' . $donnees['taille'];
				}
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT espece FROM People WHERE nom = ?');
				$reponse->execute(array($_GET['nom']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Espece : </strong> </h>' . '      ' . $donnees['espece'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT armes FROM People WHERE nom = ?');
				$reponse->execute(array($_GET['nom']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px; margin-left: 150px;"><strong>Armes</strong> </h><br>';
					echo nl2br($donnees['armes']);
				}
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT vehicules FROM People WHERE nom = ?');
				$reponse->execute(array($_GET['nom']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px; margin-left: 150px;"><strong>Vehicules</strong></h><br>';
					echo nl2br($donnees['vehicules']) . '<p></p>';
					
				}
				?> 
			<hr>
			<?php 
				if(verifyadmin() == 1){?>
					<div class ="col text-center">
						<a href="modifierperso.php?nom=<?php echo $_GET['nom']?>">Modifier</a>

						<form method="POST">
							<button type="submit" name="supprimerperso" class="btn btn-primary" data-toggle="modal" data="#exampleModal">Supprimer le personnage</button>
							<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Supprimer le personnage</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<p>Êtes-vous sûr de supprimer ce personnage ? </p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
											<button type="button" class="btn btn-primary">Oui</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div><?php
				}
			?>

			
			
		</div>	
		<div class="clearfix"></div>
	</div> 
</div>
<!-- FIN ICI -->
 <?php include 'footer.php'; ?>