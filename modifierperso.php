<?php 
	include 'navbar.php';
    verifyconnect();
    if(verifyadmin() == 0){?>
        <script>
            window.location.replace("index.php?con=1");
        </script><?php
    }
	$people = getpeople($_GET['nom']);

	if(isset($_POST['formmodifierperso'])){
		$name1 = htmlspecialchars($_POST['name1']);
		$sexe1 = htmlspecialchars($_POST['sexe1']);
		$taille1 = htmlspecialchars(($_POST['taille1']));
		$affiliations1 = htmlspecialchars($_POST['affiliations1']);
		$espece1 = htmlspecialchars($_POST['espece1']);
		$armes1 = htmlspecialchars($_POST['armes1']);
		$vehicules1 = htmlspecialchars($_POST['vehicules1']);
		$picture1 = htmlspecialchars($_POST['picture1']);
		$histoire1 = htmlspecialchars($_POST['histoire1']);

		if(!empty($_POST['name1']) AND !empty($_POST['sexe1'])AND !empty($_POST['taille1'])AND !empty($_POST['affiliations1'])
		AND !empty($_POST['espece1'])AND !empty($_POST['armes1'])AND !empty($_POST['vehicules1'])AND !empty($_POST['picture1'])AND !empty($_POST['histoire1'])){
			$addperso = $bdd->prepare("UPDATE People SET nom = ?, sexe = ?, taille=?, affiliations=?,espece=?,armes=?,vehicules=?,picture=?,description=? WHERE id=? ");
			$addperso->execute(array($name1, $sexe1, $taille1,$affiliations1,$espece1,$armes1,$vehicules1,$picture1,$histoire1,$people['id']));
			

		} else {
			$erreurajout = "Veuillez remplir tous les champs puis reessayer.";
			
		}
	}

	

?>

<!-- COMMENCEZ VOS BODY ICI -->
<div class="container">
	<div class="row">
        <div class="col">
        	<div id="wrappercentral" class="row">
        		<div class="col text-center">
        			<?php if(isset($erreurajout)){
        				echo "<strong style='color:red;'>" . $erreurajout ."</strong>";
        			}
        			?>
        			<h2>Modifier les informations de <?php echo $people['nom']; echo $people['id']; ?></h2>
        		</div>
        	</div>
        	
            <div id="wrappercentral"class="row">
        		<div class="col text-justify">
                <!-- Modifier les info  du personnage-->
                <form action="" method="post">
            		<div class="form-group">
            			<label for="nom">Nom :</label>
                        <input type="name" class="form-control" placeholder="" name="name1" id="name1" value="<?php 
                        echo $people['nom'];
                        ?>">
            		</div>
            		<div class="form-group">
            			<label for="sexe">sexe :</label>
            			<select name="sexe1" id="sexe1">
                            <option value="H">H</option>
                            <option value="F">F</option>
                        </select>
            		</div>
            		<div class="form-group">
            			<label for="taille">Taille:</label>
                        <input type="height" class="form-control" placeholder="" name="taille1" id="taille1" value="<?php
                        echo $people['taille'];
                        ?>">
            		</div>
            		<div class="form-group">
            			<label for="affiliations">Affiliations :</label>
                        <textarea name="affiliations1" id="affiliations1" cols="12"><?php
                        echo $people['affiliations'];
                        ?></textarea> 
            		</div>
            		<div class="form-group">
            			<label for="espece">espece :</label>
                        <input type="species" class="form-control" placeholder=""  name ="espece1" id="espece1" value ="<?php
                        echo $people['espece'];
                        ?>">
            		</div>
            		<div class="form-group">
            			<label for="armes">armes:</label>
                        <textarea name="armes1" id="armes1" cols="12"><?php
                        echo $people['armes'];
                        ?></textarea> 
            		</div>
                    <div class="form-group">
            			<label for="vehicules">vehicules:</label>
                        <textarea name="vehicules1" id="vehicules1"><?php
                        echo $people['vehicules'];
                        ?></textarea> 
            		</div>
                    <div class="form-group">
            			<label for="picture">picture :</label>
                        <input type="picture" class="form-control" placeholder="" name ="picture1" id="picture1" value ="<?php
                        echo $people['picture'];
                        ?>">
            		</div>
                    <div class="form-group">
            			<label for="histoire">Histoire:</label>
                        <textarea name="histoire1" id="histoire1" cols="12"><?php
                        echo $people['description'];
                        ?></textarea> 
            		</div>

            		<button type="submit" name="formmodifierperso" class="btn btn-primary">Envoyer</button>
            				
            	</form>   
		</div>
	</div> 
</div>



<?php

    include 'footer.php'

?>