<?php 
	include 'navbar.php';
  	verifyconnect();
	$planete = getplanete($_GET['planete']);
?>
<!-- COMMENCEZ VOS BODY ICI -->
<div class="container bg-light">
	<div class="row">
		<div class="col text-center">
			<h2>Bienvenue sur <?php echo $planete['name'];?></h2>
		</div>
	</div><hr>
	<div class="row text-justify">
		<div class="col">
			<img width="100%" src="<?php echo $planete['picture'];?>" alt="<?php echo $planete['name'];?>">
		</div>
		<div class="col-md-8">
			<h4>Description de <?php echo $planete['name'];?>:</h4>
			<?php echo $planete['description'];?>
		</div>
	</div><hr>
	<div class="row text-center">
		<div class="col">
			<h2>Détails de la planète :</h2>
		</div>
	</div>
	<div class="row text-justify">
		<div class="col-12 col-md text-center">
			<strong>Diamètre de la planète : </strong><?php echo $planete['diameter']; ?>.<br>
			<strong>Période de rotation : </strong><?php if($planete['rotation']==0){
				echo "N/A";
			} else {
				echo $planete['rotation'];
			}?>.<br>
			<strong>Période orbitale : </strong><?php if($planete['orbite']==0){
				echo "N/A";
			} else {
				echo $planete['orbite'];
			}?>.<br>
		</div>
		<div class="col-12 col-md text-center">
			<strong>Population : </strong><?php echo $planete['population']; ?>.<br>
			<strong>Climat de la planète : </strong><?php echo $planete['climat']; ?>.<br>
			<strong>Intensité de la gravité : </strong><?php if($planete['gravity']==0){
				echo "N/A";
			} else {
				echo $planete['gravity'];
			}?>.<br>
		</div>
	</div><hr>
	<div class="row">
		<div class="col text-center">
			<h2>La planète apparait dans les films :</h2>
		</div>
	</div>
	<div class="row text-center">
		<?php
			$con = connectionbdd();
			$reqlistfilm = $con->prepare("SELECT * FROM Film INNER JOIN FilmPlanets ON Film.episode = FilmPlanets.id_film WHERE id_planet = ?");
			$reqlistfilm->execute(array($_GET['planete']));
			if($reqlistfilm->rowCount() != 0){
				while($listfilm = $reqlistfilm->fetch()){ ?>
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">
				            	<h4 class="card-title"><?php echo $listfilm['titre']; ?></h4>
				          	</div>
				          	<a href="film.php?episode=<?php echo $listfilm['episode']; ?>"><img width="100%" class="card-img-bottom" src="<?php echo $listfilm['picture']; ?>"alt="Card image cap"></a>
						</div>
					</div><?php
				}
			} else { ?>
				<div class="col text-center">
					<h4>Désolé cette planète n'apparait dans aucun film.</h4>
				</div><?php
			}?>
	</div>
<!-- FIN ICI -->
</div>
<?php include 'footer.php';?>