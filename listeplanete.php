<?php
  	include 'navbar.php';
  	verifyconnect();
?>
<div id="bgplanete" class="container-fluid" style="background-image: url(images/etoiles.jpg);">
	<div id= "bgplanete" class="container" style="background-image: url(images/etoiles.jpg);">
	  	<div class="row">
		    <div class="col text-center" style="color:white;">
		      	<h2>Voici la liste des planètes de Star Wars :</h2>
		    </div>
	  	</div>
		<div class="row">
		<?php
		$compteur = 0;
		$listplanet = $bdd->query("SELECT * FROM Planet");
		while($planete = $listplanet->fetch()){
		  	if($compteur%3 == 0 && $compteur != 0){
		  		?></div><br><div class="row"><?php
		  	}?>
		    <div class="col-md-4 text-center">
	        	<h3 style="color:white;"><?php echo $planete['name']; ?></h3>
	          	<a href="planete.php?planete=<?php echo $planete['id']; ?>" title="Clique pour découvrir sa description :)"><img id="planete" width="100%" src="<?php echo $planete['picture']; ?>" href="planete.php" alt=<?php echo $planete['name']; ?>></a>
		    </div>
		<?php 
		$compteur++;
		} ?>
		</div>
	</div>
</div>
<?php include 'footer.php';?>