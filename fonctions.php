<?php
try {
	$bdd = new PDO('mysql:host=localhost;dbname=bdd_9_7;charset=utf8;port=3306', 'grp_9_7', 'esh3JeeT2l'); 
	array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION); 
} catch(Exception $e) { 
	die('Erreur : ' . $e->getMessage()); 
};

function connectionbdd(){
	$con = new PDO('mysql:host=localhost;dbname=bdd_9_7;charset=utf8;port=3306', 'grp_9_7', 'esh3JeeT2l'); 
	array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
	return $con;
}
/****************** INSCRIPTION ********************/
if(isset($_POST['forminscription'])){
	$con = connectionbdd();
	$pseudo1 = htmlspecialchars($_POST['pseudo1']);
	$email1 = htmlspecialchars($_POST['email1']);
	$mdp1 = sha1($_POST['mdp1']);
	if(!empty($_POST['pseudo1']) AND !empty($_POST['email1'])AND !empty($_POST['mdp1'])){
		$taillepseudo = strlen($pseudo1);
		if($taillepseudo <= 255){
			if(filter_var($email1, FILTER_VALIDATE_EMAIL)){
				$reqmail = $con->prepare("SELECT * FROM users WHERE email = ?");
				$reqmail -> execute(array($email1));
				$exist_mail = $reqmail->rowCount();
				if($exist_mail == 0){
					$insertmbr = $con->prepare("INSERT INTO users(pseudo, mot_de_passe, email) VALUES(?,?,?)");
					$insertmbr->execute(array($pseudo1, $mdp1, $email1));
					$msgcon = "Compte créé avec succés, veuillez vous connecter.";
					$opencon = 1;
				} else {
					$msginsc = "Désolé, cette utilisateur existe déjà pour votre adresse mail.";
					$openinsc = 1;
				}
			}
		} else {
			$msginsc = "Veuillez rentrer un pseudo de moins de 255 caractères.";
			$openinsc = 1;
		}
	} else {
		$msginsc = "Veuillez remplir tous les champs puis reessayer.";
		$openinsc = 1;
	}
}

/********************* Connexion ********************/
if(isset($_POST['formconnection'])){
	$con = connectionbdd();
	$pseudo2 = htmlspecialchars($_POST['pseudo2']);
	$mdp2 = sha1($_POST['mdp2']); 
	if(!empty($_POST['pseudo2']) AND !empty($_POST['mdp2'])){
		$requser = $con->prepare("SELECT * FROM users WHERE pseudo=? AND mot_de_passe=?");
		$requser->execute(array($pseudo2, $mdp2));
		$exist_user = $requser->rowCount();
		if($exist_user == 1){
			$userinfo = $requser->fetch();
			$idTemp = $userinfo['idUsers'];
			setcookie("idSession", $idTemp, time()+24*3600, null, null, false, true);
			header("Refresh: 0");
			$opencon = 0;
		} else {
			$msgcon = "Cette utilisateur n'existe pas, veuillez vérifier vos informations et essayer à nouveau.";
			$opencon = 1;
		}
	} else {
		$msgcon = "Veuillez remplir tous les champs puis reessayer.";
		$opencon = 1;
	}
}

/******************************Recherche*************/
if(isset($_POST['recherche'])){
	header("Location: recherche.php?recherche=".$_POST['txtrecherche']);
}

function getfilm($num){
	$con = connectionbdd();
	$reqfilm = $con->prepare("SELECT * FROM Film WHERE episode = ?");
	$reqfilm->execute(array($num));
	$film = $reqfilm->fetch();
	return $film;
}

function getplanete($idplanete){
	$con = connectionbdd();
	$reqplanete = $con->prepare("SELECT * FROM Planet WHERE id = ?");
	$reqplanete->execute(array($idplanete));
	$planete = $reqplanete->fetch();
	return $planete;
}

function getpeople($peoplename){
	$con = connectionbdd();
	$reqpeople = $con->prepare("SELECT * FROM People WHERE nom = ?");
	$reqpeople->execute(array($peoplename));
	$people = $reqpeople->fetch();
	return $people;
}

function getstarship($starshipname){
	$con = connectionbdd();
	$reqstarship = $con->prepare("SELECT * FROM Starship WHERE name = ?");
	$reqstarship->execute(array($starshipname));
	$starship = $reqstarship->fetch();
	return $starship;
}


function afficherdate($date){
	for ($i=2; $i>=0; $i--) { 
		if($i == 1){
			switch ($date[$i]){
				case 1: echo " - Janvier - ";
				break;
				case 2: echo " - Fevrier - ";
				break;
				case 3: echo " - Mars - ";
				break;
				case 4: echo " - Avril - ";
				break;
				case 5: echo " - Mai - ";
				break;
				case 6: echo " - Juin - ";
				break;
				case 7: echo " - Juillet - ";
				break;
				case 8: echo " - Aout - ";
				break;
				case 9: echo " - Septembre - ";
				break;
				case 10: echo " - Octobre - ";
				break;
				case 11: echo " - Novembre - ";
				break;
				case 12: echo " - Décembre - ";
				break;
				default:
				break;
			}
		} else {
			echo $date[$i];
		}
	}
}

function verifyvote($userid, $idfilm){
	$con = connectionbdd();
	$reqnote = $con->prepare("SELECT * FROM stars WHERE userid = ? AND film = ?");
	$reqnote->execute(array($userid, $idfilm));
	$noteverifier = $reqnote->rowCount();
	return $noteverifier;
}

function verifylisteperso($userid, $idfilm){
	$con = connectionbdd();
	$reqnote = $con->prepare("SELECT * FROM stars WHERE userid = ? AND film = ?");
	$reqnote->execute(array($userid, $idfilm));
	$noteverifier = $reqnote->rowCount();
	return $noteverifier;

}

function getvote ($userid, $idfilm){
	$con = connectionbdd();
	$reqnote = $con->prepare("SELECT * FROM stars WHERE userid = ? AND film = ?");
	$reqnote->execute(array($userid, $idfilm));
	$note = $reqnote->fetch();
	return $note;
}

function verifyconnect(){
	if(!isset($_COOKIE['idSession'])){?>
       	<script>
       		window.location.replace("index.php?con=0");
       	</script><?php
       	return 0;
	} else {
		return 1;
	}
}

function verifyadmin(){
	if(verifyconnect() == 1){
		$con = connectionbdd();
		$reqadmin = $con->prepare("SELECT type_connexion FROM users WHERE idUsers = ?");
		$reqadmin->execute(array($_COOKIE['idSession']));
		$admin = $reqadmin->fetch();
		if(isset($admin['type_connexion'])){
			if($admin['type_connexion'] == 1){
				return 1;
			} else {
				return 0;
			}
		}
	}
}

function getbestfilm(){
	$con = connectionbdd();
	$reqbest = $con->query("SELECT * FROM stars GROUP BY film ORDER BY COUNT(userid) DESC");
	$best = $reqbest->fetch();
	return $best;
}

function countvotes($idfilmvote){
	$con = connectionbdd();
	$votes = $con->prepare("SELECT userid FROM stars WHERE film = ?");
	$votes->execute(array($idfilmvote));
	$nbvotes = $votes->rowCount();
	return $nbvotes;
}
?>