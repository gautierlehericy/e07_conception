		</main>
	</body>	
	<script>
        //Javascript pour afficher un modal
        $(document).ready(function(){
            $("#triggerconnection").click(function(){
                $("#modalconnection").modal('show');
            });
            $("#triggerinscription").click(function(){
                $("#modalinscription").modal('show');
            });
        });
    </script>
    <?php
    if(isset($opencon) && $opencon == 1){
        echo $opencon;?>
        <script>
            //Javascript pour afficher un modal
            $(document).ready(function(){
                $("#modalconnection").modal('show');
            });
        </script><?php
    }
    if(isset($openinsc) && $openinsc == 1){
        echo $openinsc;?>
        <script>
            //Javascript pour afficher un modal
            $(document).ready(function(){
                $("#modalinscription").modal('show');
            });
        </script><?php
    }?>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script>
        window.onload = () => {
            let menus = document.querySelectorAll(".dropdown-submenu a.dropdown-toggle")
            for(let menu of menus){
                menu.addEventListener("click", function(e){
                    e.stopPropagation()
                    e.preventDefault()
                    // On masque tous les UL de sous menus
                    let sousmenus = document.querySelectorAll(".multi .dropdown-menu")
                    for(let sousmenu of sousmenus){
                        sousmenu.style.display = "none"
                    }
                    this.nextElementSibling.style.display = "initial"
                })
            }
        }
    </script>
	<footer class="page-footer bg-dark" id="footer" style="background-image: url(images/etoiles.jpg);">	
		<div class="container text-center" style="color:white">
			<div class="row">
				<div class="col"><br>
					<h3>Suivez Star Wars sur les réseaux sociaux :</h3>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<a href="https://www.instagram.com/starwars/" id="icones" target="_blank" class="fa fa-instagram"></a>
					<a href="https://www.facebook.com/StarWars.fr/" id="icones" target="_blank" class="fa fa-facebook"></a>
					<a href="https://twitter.com/starwars" id="icones" target="_blank" class="fa fa-twitter"></a>
					<a href="https://www.youtube.com/user/starwars" id="icones" target="_blank" class="fa fa-youtube"></a>
				</div>
			</div>
		</div>
		<div class="footer-copyright text-center py-3">
			<p id="copyright" style="color:white">&copy; Gautier LEHERICY & Tom LIM & Austin MOREL.</p>
		</div>
	</footer>
</html>