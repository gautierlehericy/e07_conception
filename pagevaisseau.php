<?php 
        include 'navbar.php';
        verifyconnect();
?>
<!-- COMMENCEZ VOS BODY ICI -->
<div class="container">
	<div class="row">
        <div class="col-md-8">
        	<div id="wrappercentral" class="row">
        		<div class="col text-center">
        			<h2>Histoire de <?php echo $_GET['name']; ?> </h2>
        		</div>
        	</div>
        	<div id="wrappercentral"class="row">
        		<div class="col text-justify">
        			<?php 
        				$starship = getstarship($_GET['name']); 
        				echo nl2br($starship['description']);
        			?>

        		</div>
        	</div>
        </div>
        <div class ="col-md-4" style ="background: #dbd9d9;">
			<img src="<?php 
				echo $starship['picture'];
			?>
			" class="pt-3 img-fluid" style= "width: 350px; no-repeat center center" alt="Circle image" class="img-circle"/>
			<h3>
				<?php
					echo $_GET['name'];
				?>
			</h3>
			<hr>
			<h style="font-size: 17px; margin-left: 150px;"><strong>Films </strong> </h><br>
			<?php 
				$liste = $bdd->prepare("SELECT * FROM Film INNER JOIN StarshipsInFilms ON Film.episode = StarshipsInFilms.id_film INNER JOIN Starship ON StarshipsInFilms.id_starship = Starship.id WHERE name = ?");
				$lien = $bdd->prepare("SELECT episode FROM FILM INNER JOIN  StarshipsInFilms ON Film.episode =  StarshipsInFilms.id_film	INNER JOIN Starship ON  StarshipsInFilms.id_starship = Starship.id WHERE name =?");
				
				$liste->execute(array($_GET['name']));
				$lien->execute(array($_GET['name']));
				while ($listefilm = $liste->fetch()){?>
					<a href="film.php?episode=<?php echo $listefilm['episode'];?>" style="color: #730505; margin-left: 10px;"><?php echo $listefilm['titre'];?></a><br><?php
				}
			?>
			<hr>
			<h style="font-size: 17px; margin-left: 150px;"><strong>Affiliations</strong> </h><br>
			
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT starship_class FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Classe de vaisseau : </strong> </h>' . '      ' . $donnees['starship_class'];
				}

			 
			
				?> 
			<hr>
			
				<?php 

				$reponse = $bdd->prepare('SELECT model FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Modèle : </strong> </h>' . '      ' . $donnees['model'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT manufacturer FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Fabricant : </strong> </h>' . '      ' . $donnees['manufacturer'];
				}
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT cost_in_credits FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Coût en crédits : </strong> </h>' . '      ' . $donnees['cost_in_credits'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT length FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Longueur : </strong> </h>' . '      ' . $donnees['length'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT max_atmosphering_speed FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Vitesse atmosphérique max : </strong> </h>' . '      ' . $donnees['max_atmosphering_speed'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT crew FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Equipage : </strong> </h>' . '      ' . $donnees['crew'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT passengers FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Passagers : </strong> </h>' . '      ' . $donnees['passengers'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT cargo_capacity FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Capacité de chargement : </strong> </h>' . '      ' . $donnees['cargo_capacity'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT consumables FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Autonomie : </strong> </h>' . '      ' . $donnees['consumables'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT hyperdrive_rating FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Rang de vitesse lumière : </strong> </h>' . '      ' . $donnees['hyperdrive_rating'];
				}

			 
			
				?> 
			<hr>
			<?php 

				$reponse = $bdd->prepare('SELECT MGLT FROM Starship WHERE name = ?');
				$reponse->execute(array($_GET['name']));
				while ($donnees = $reponse->fetch()){
					echo '<h style="font-size: 17px;"><strong>Megalumière par heure : </strong> </h>' . '      ' . $donnees['MGLT'];
				}

			 
			
				?> 
			<hr>
			
		</div>	
		<div class="clearfix"></div>
	</div> 
</div>
<!-- FIN ICI -->
 <?php include 'footer.php'; ?>