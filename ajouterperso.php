<?php 
	include 'navbar.php'; 
    verifyconnect();
    if(verifyadmin() == 0){?>
        <script>
            window.location.replace("index.php?con=1");
        </script><?php
    }
	if(isset($_POST['formajouterperso'])){
		$name1 = htmlspecialchars($_POST['name1']);
		$sexe1 = htmlspecialchars($_POST['sexe1']);
		$taille1 = htmlspecialchars(($_POST['taille1']));
		$affiliations1 = htmlspecialchars($_POST['affiliations1']);
		$espece1 = htmlspecialchars($_POST['espece1']);
		$armes1 = htmlspecialchars($_POST['armes1']);
		$vehicules1 = htmlspecialchars($_POST['vehicules1']);
		$picture1 = htmlspecialchars($_POST['picture1']);
		$histoire1 = htmlspecialchars($_POST['histoire1']);

		if(!empty($_POST['name1']) AND !empty($_POST['sexe1'])AND !empty($_POST['taille1'])AND !empty($_POST['affiliations1'])
		AND !empty($_POST['espece1'])AND !empty($_POST['armes1'])AND !empty($_POST['vehicules1'])AND !empty($_POST['picture1'])AND !empty($_POST['histoire1'])){
			$addperso = $bdd->prepare("INSERT INTO People(nom,sexe,taille,affiliations,espece,armes,vehicules, picture,description) VALUES (?,?,?,?,?,?,?,?,?)");
			$addperso->execute(array($name1, $sexe1, $taille1,$affiliations1,$espece1,$armes1,$vehicules1,$picture1,$histoire1));
			

		} else {
			$erreurajout = "Veuillez remplir tous les champs puis reessayer.";
			
		}
	}
?>
<!-- COMMENCEZ VOS BODY ICI -->
<div class="container">
	<div class="row">
        <div class="col">
        	<div id="wrappercentral" class="row">
        		<div class="col text-center">
        			<?php if(isset($erreurajout)){
        				echo "<strong style='color:red;'>" . $erreurajout ."</strong>";
        			}
        			?>
        			<h2>Ajouter des personnages ici:  </h2>
        		</div>
        	</div>
        	
            <div id="wrappercentral"class="row">
        		<div class="col text-justify">
                <!-- Ajouter la description du personnage-->
                <form action="" method="post">
            		<div class="form-group">
            			<label for="nom">Nom :</label>
            			<input type="name" class="form-control" placeholder="Entrez le nom" name="name1" id="name1">
            		</div>
            		<div class="form-group">
            			<label for="sexe">sexe :</label>
            			<select name="sexe1" id="sexe1">
                            <option value="H">H</option>
                            <option value="F">F</option>
                        </select>
            		</div>
            		<div class="form-group">
            			<label for="taille">Taille:</label>
            			<input type="height" class="form-control" placeholder="Entrez sa taille" name="taille1" id="taille1">
            		</div>
            		<div class="form-group">
            			<label for="affiliations">Affiliations :</label>
            			<textarea name="affiliations1" id="affiliations1" cols="12"></textarea> 
            		</div>
            		<div class="form-group">
            			<label for="espece">espece :</label>
            			<input type="species" class="form-control" placeholder="Entrez son espece" name ="espece1" id="espece1">
            		</div>
            		<div class="form-group">
            			<label for="armes">armes:</label>
            			<textarea name="armes1" id="armes1" cols="12"></textarea> 
            		</div>
                    <div class="form-group">
            			<label for="vehicules">vehicules:</label>
            			<textarea name="vehicules1" id="vehicules1" cols="12"></textarea> 
            		</div>
                    <div class="form-group">
            			<label for="picture">picture :</label>
            			<input type="picture" class="form-control" placeholder="Entrez la direction de l'image" name ="picture1" id="picture1">
            		</div>
                    <div class="form-group">
            			<label for="histoire">Histoire:</label>
            			<textarea name="histoire1" id="histoire1" cols="12"></textarea> 
            		</div>

            		<button type="submit" name="formajouterperso" class="btn btn-primary">Submit</button>
            				
            	</form>   
		</div>
	</div> 
</div>

<?php

// On récupère les champs 
/*
if(isset($_POST['histoire'])){
    $histoire = $_POST['histoire'];
}else{
  $histoire="''";
};

if(isset($_POST['picture'])){
    $picture = $_POST['picture'];
}else{
  $picture="''";
};

if(isset($_POST['nom'])){
    $nom = $_POST['nom'];
}else{
  $nom="''";
};

if(isset($_POST['affiliation'])){
    $affiliation = $_POST['affiliation'];
}else{
  $affiliation="''";
};

if(isset($_POST['sexe'])){
    $sexe = $_POST['sexe'];
}else{
  $sexe="NULL";
};

if(isset($_POST['taille'])){
    $taille = $_POST['taille'];
}else{
  $taille="NULL";
};

if(isset($_POST['espece'])){
    $espece = $_POST['espece'];
}else{
  $espece="''";
};

if(isset($_POST['armes'])){
    $armes = $_POST['taille'];
}else{
  $armes="''";
};

if(isset($_POST['vehicules'])){
    $vehicules = $_POST['vehicules'];
}else{
  $vehicules="''";
};
*/

?>

<!-- FIN ICI -->
<?php include 'footer.php'; ?>
 
 