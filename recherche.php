<?php
	include 'navbar.php';
	$con = connectionbdd();
	$recherche = htmlspecialchars($_GET['recherche']);
?>
<div class="container bg-light">
	<div class="row">
		<div class="col text-center">
			<strong><h2>Voici les résultats disponibles pour la recherche : <?php echo $_GET['recherche'];?> :</h2></strong>
		</div>
	</div>
	<div class="row">
		<div class="col text-justify">
			<?php 
			if(empty($recherche)){
				echo "<h3>Désolé, nous ne trouvons rien que correspond à votre recherche... <br>Vous pouvez essayer de chercher un autre objet de la galaxie ou bien cliquer <a href='index.php'>ici</a> pour retourner à l'accueil";
			} else {
				$cpt = 0;
				echo "</div></div>";
				$recherchefilm = $con->query("SELECT * FROM Film WHERE Film.titre LIKE '%$recherche%'");
				$resultfilm = $recherchefilm->fetch();
				$rechercheperso = $con->query("SELECT * FROM People WHERE People.nom LIKE '%$recherche%'");
				$resultperso = $rechercheperso->fetch();
				$rechercheplanete = $con->query("SELECT * FROM Planet WHERE Planet.name LIKE '%$recherche%'");
				$resultplanete = $rechercheplanete->fetch();
				$recherchevehicule = $con->query("SELECT * FROM Starship WHERE Starship.name LIKE '%$recherche%'");
				$resultvehicule = $recherchevehicule->fetch();
				if(isset($resultfilm['titre'])){
					$cpt++;?>
					<div class="row">
						<div class="col">
							<p>Notre droïde a trouvé : <strong><a style="color:grey;"href="film.php?episode=<?php echo $resultfilm['episode'];?>"><?php echo $resultfilm['titre'];?></a></strong></p>
						</div>
					</div><?php
					while($resultfilm = $recherchefilm->fetch()){?>
						<div class="row">
							<div class="col">
								<p>Notre droïde a trouvé : <strong><a style="color:grey;"href="film.php?episode=<?php echo $resultfilm['episode'];?>"><?php echo $resultfilm['titre'];?></a></strong></p>
							</div>
						</div><?php
					}
				} if(isset($resultperso['nom'])) {
					$cpt++;?>
					<div class="row">
						<div class="col">
							<p>Notre droïde a trouvé : <strong><a style="color:grey;"href="pagepersonnage.php?nom=<?php echo $resultperso['nom'];?>"><?php echo $resultperso['nom'];?></a></strong></p>
						</div>
					</div><?php
					while($resultperso = $rechercheperso->fetch()){?>
						<div class="row">
							<div class="col">
								<p>Notre droïde a trouvé : <strong><a style="color:grey;"href="pagepersonnage.php?nom=<?php echo $resultperso['nom'];?>"><?php echo $resultperso['nom'];?></a></strong></p>
							</div>
						</div><?php
					}
				} if(isset($resultplanete['name'])){
					$cpt++;?>
					<div class="row">
						<div class="col">
							<p>Notre droïde a trouvé : <strong><a style="color:grey;"href="planete.php?planete=<?php echo $resultplanete['id'];?>"><?php echo $resultplanete['name'];?></a></strong></p>
						</div>
					</div><?php
					while($resultplanete = $rechercheperso->fetch()){?>
						<div class="row">
							<div class="col">
								<p>Notre droïde a trouvé : <strong><a style="color:grey;"href="planete.php?planete=<?php echo $resultplanete['id'];?>"><?php echo $resultplanete['name'];?></a></strong></p>
							</div>
						</div><?php
					}
				} if(isset($resultvehicule['name'])){
					$cpt++;?>
					<div class="row">
						<div class="col">
							<p>Notre droïde a trouvé : <strong><a style="color:grey;"href="pagevaisseau.php?name=<?php echo $resultvehicule['name'];?>"><?php echo $resultvehicule['name'];?></a></strong></p>
						</div>
					</div><?php
					while($resultvehicule = $rechercheperso->fetch()){?>
						<div class="row">
							<div class="col">
								<p>Notre droïde a trouvé : <strong><a style="color:grey;"href="planete.php?planete=<?php echo $resultplanete['id'];?>"><?php echo $resultplanete['name'];?></a></strong></p>
							</div>
						</div><?php
					}
				} else if($cpt == 0){
					echo "<h3>Désolé, nous ne trouvons rien que correspond à votre recherche... <br>Vous pouvez essayer de chercher un autre objet de la galaxie ou bien cliquer <a href='index.php'>ici</a> pour retourner à l'accueil</h3>";
				}
			}?>
		</div>
	</div>
</div>
<?php
	include 'footer.php';
?>