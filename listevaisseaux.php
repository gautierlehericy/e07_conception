<?php
  	include 'navbar.php';
  	verifyconnect();

?>
<div class="container bg-light">
	<div class="row">
		<div class="col text-center">
			<h2>Catégories :</h2>
		</div>
    </div>
    <div class="row">
        <div class="col">
            <ul>
				<?php 
					$listevaisseaux = $bdd->query('SELECT nomAffiliation FROM Affiliation');
					while($affichervaisseaux = $listevaisseaux->fetch())
					{
						echo'<li><a href="vaisseaux.php?nomAffiliation='.  $affichervaisseaux['nomAffiliation'] . '" style="color: black;">' . $affichervaisseaux['nomAffiliation'] . '</a></li>';
					};
				?>
            </ul>
        </div>
    </div>
	<div class="col text-center">
		<h2>Notre top 3 des vaisseaux :</h2>
	</div>
	<!-- Top 3 Vaisseaux -->
	<div class="row">
		<!-- Vaisseau 1 -->
		<div class="col-md-4">
			<div class="card-deck">
				<div class="card">
					<a href="pagevaisseau.php?name=Millennium%20Falcon" title="Clique pour découvrir sa description :)"><img class="card-img-top" src="images/Vaisseaux/Millennium Falcon.png" href="pagevaisseau.php?name=Millennium%20Falcon" alt="Card image cap"></a>
					<div class="card-body">
						<h5 class="card-title">1 - Faucon Millenium</h5>
					</div>
				</div>
			</div>
		</div>
		<!-- Vaisseau 2 -->
		<div class="col-md-4">
			<div class="card-deck">
				<div class="card">
					<a href="pagevaisseau.php?name=X-wing" title="Clique pour découvrir sa description :)"><img class="card-img-top" src="images/Vaisseaux/X-wing.png" href="pagevaisseau.php?name=X-wing" alt="Card image cap"></a>
					<div class="card-body">
						<h5 class="card-title">2 - X-wing</h5>
					</div>
				</div>
			</div>
		</div>
		<!-- Vaisseau 3 -->
		<div class="col-md-4">
			<div class="card-deck">
				<div class="card">
					<a href="pagevaisseau.php?name=TIE%20Advanced%20x1" title="Clique pour découvrir sa description :)"><img class="card-img-top" src="images/Vaisseaux/TIE Advanced x1.png" href="pagevaisseau.php?name=TIE%20Advanced%20x1" alt="Card image cap"></a>
					<div class="card-body">
						<h5 class="card-title">3 - TIE Advanced x1</h5>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
</div>
<!-- FIN ICI -->
<?php include 'footer.php';?>