<?php 
	include'navbar.php';
  	verifyconnect(); 
?>
<!-- COMMENCEZ VOS BODY ICI -->
<div class="container bg-light">
	<div class="row">
		<div class="col text-center">
			<h2><?php echo $_GET['nomAffiliation'] ?></h2>
		</div>
		<div class="row">

	<?php
	$listevaisseau = $bdd->prepare('SELECT * FROM Starship INNER JOIN StarshipsInAffiliation ON Starship.id = StarshipsInAffiliation.id INNER JOIN Affiliation ON StarshipsInAffiliation.id_affilia = Affiliation.id_affiliation WHERE nomAffiliation = ? ');
	$listevaisseau->execute(array($_GET['nomAffiliation']));
	while($data1 = $listevaisseau->fetch()){
	  		?>
	    <div class="col-md-4">
	        <div class="card">
	          	<a href="pagevaisseau.php?name=<?php echo $data1['name']; ?> " title="Clique pour découvrir sa description :)"><img class="card-img-top" src="<?php echo $data1['picture']; ?>" href="" alt="Card image cap"></a>
	          	<div class="card-body">
	            	<h5 class="card-title"><?php echo $data1['name']; ?></h5>
	            	<p class="card-text"></p>
	          	</div>
	        </div>
	    </div>
	<?php } ?>
	</div>
	</div>
<!-- FIN ICI -->
</div>
<?php include 'footer.php';?>