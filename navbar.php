<?php 
	include 'fonctions.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet"  href="CSS/style.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <title> La revanche du site </title>
        <link rel="shortcut icon" href="images/sabre.png"/>
    </head>
    <body>
        <main role="main">
            <div class="container-fluid text-center" style="background-image: url(images/etoiles.jpg)">
                <div class="row">
                    <div class="col">
                        <a href="index.php"><img id="logoaccueil" class="pt-4 pb-4" width="100%"src="images/starwars.png" alt="star wars"></a>
                    </div>
                </div>
            </div>
        	<nav id="navbar" class="navbar navbar-expand-lg navbar-dark sticky-top">
            	<button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                	<span class="navbar-toggler-icon" style="color: white;"></span>
                	<span style="color: white">Menu</span>
            	</button>
            	<div class="collapse navbar-collapse" id="navbarNavDropdown">
                	<ul class="navbar-nav">
                    	<li class="nav-item active">
                        	<a class="nav-link" href="index.php">Accueil</a>
                        </li>
                        <!-- Navbar Personnages -->
                        <li class="nav-item dropdown active">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Personnages</a>
							<ul class="dropdown-menu multi" aria-labelledby="navbarDropdown">
	                            <li class="dropdown-submenu">
								    <a class="dropdown-item dropdown-toggle" style="color: #6c757d" href="#">Trilogie Originale (1977-1983)</a>
	                                <ul class="dropdown-menu">
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=4">Star Wars épisode IV : Un Nouvel Espoir</a>

	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=5">Star Wars épisode V : L'Empire contre-attaque</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=6">Star Wars épisode VI : Le Retour du Jedi</a>
	                                    </li>
	                                </ul>
	                            </li>
	                            <li class="dropdown-submenu">
								    <a class="dropdown-item dropdown-toggle" style="color: #6c757d" href="#">Prélogie (1999-2005)</a>
	                                <ul class="dropdown-menu">
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=1">Star Wars épisode I : La Menace Fantôme</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=2">Star Wars épisode II : L'Attaque des Clones</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=3">Star Wars épisode III : La Revanche des Sith</a>
	                                    </li>
	                                </ul>
								</li>
	                            <li class="dropdown-submenu">
	                                <a class="dropdown-item dropdown-toggle" style="color: #6c757d" href="#">Troisième Trilogie (2015-2019)</a>
	                                <ul class="dropdown-menu">
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=7">Star Wars épisode VII : Le Réveil de la Force</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=8">Star Wars épisode VIII : Les Derniers Jedi</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=9">Star Wars épisode IX : L'Ascension de Skywalker</a>
	                                    </li>
	                                </ul>
								</li>
	                            <li class="dropdown-submenu">
	                                <a class="dropdown-item dropdown-toggle" style="color: #6c757d" href="#">Spin-Off</a>
	                                <ul class="dropdown-menu">
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=12">Star Wars : The Clone Wars</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=10">Rogue One: A Star Wars Story</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="listepersonnage.php?episode=11">Solo: A Star Wars Story</a>
	                                    </li>
	                                </ul>
	                            </li>
							</ul>
					    </li>
                        <!-- Fin Navbar Personnages -->
                        <!-- Navbar des films -->
                        <li class="nav-item dropdown active">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Films</a>
							<ul class="dropdown-menu multi" aria-labelledby="navbarDropdown">
	                            <li class="dropdown-submenu">
								    <a class="dropdown-item dropdown-toggle" style="color: #6c757d" href="#">Trilogie Originale (1977-1983)</a>
	                                <ul class="dropdown-menu">
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=4">Star Wars épisode IV : Un Nouvel Espoir</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=5">Star Wars épisode V : L'Empire contre-attaque</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=6">Star Wars épisode VI : Le Retour du Jedi</a>
	                                    </li>
	                                </ul>
	                            </li>
	                            <li class="dropdown-submenu">
								    <a class="dropdown-item dropdown-toggle" style="color: #6c757d" href="#">Prélogie (1999-2005)</a>
	                                <ul class="dropdown-menu">
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=1">Star Wars épisode I : La Menace Fantôme</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=2">Star Wars épisode II : L'Attaque des Clones</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=3">Star Wars épisode III : La Revanche des Sith</a>
	                                    </li>
	                                </ul>
								</li>
	                            <li class="dropdown-submenu">
	                                <a class="dropdown-item dropdown-toggle" style="color: #6c757d" href="#">Troisième Trilogie (2015-2019)</a>
	                                <ul class="dropdown-menu">
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=7">Star Wars épisode VII : Le Réveil de la Force</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=8">Star Wars épisode VIII : Les Derniers Jedi</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=9">Star Wars épisode IX : L'Ascension de Skywalker</a>
	                                    </li>
	                                </ul>
								</li>
	                            <li class="dropdown-submenu">
	                                <a class="dropdown-item dropdown-toggle" style="color: #6c757d" href="#">Spin-Off</a>
	                                <ul class="dropdown-menu">
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=12">Star Wars : The Clone Wars</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=10">Rogue One: A Star Wars Story</a>
	                                    </li>
	                                    <li>
	                                        <a class="dropdown-item" href="film.php?episode=11">Solo: A Star Wars Story</a>
	                                    </li>
	                                </ul>
	                            </li>
							</ul>
                        </li>
                        <!-- Fin Navbar films -->
                        <!-- Navbar des planètes -->
                        <li class="nav-item active">
                        	<a class="nav-link" href="listeplanete.php">Planètes</a>
                        </li>
                        <!-- Fin Navbar des planètes -->
                        <!-- Navbar des Vaisseaux -->
						<li class="nav-item active">
                        	<a class="nav-link" href="listevaisseaux.php">Vaisseaux</a>
                        </li>
                        <!-- Fin Navbar vaisseaux -->
                    </ul>
                    <form class="form-inline" method="post">
	                    <!-- Barre de recherche -->
						<input style="max-width:200px;" name="txtrecherche" class="form-control mr-2" type="text" placeholder="Saisir un mot" aria-label="Rechercher">
	                    <button name="recherche" class="btn btn-outline-danger mr-2" type="submit">Rechercher</button>
	                </form>
                </div>
                <!-- Boutons connection/s'enregistrer -->
               	<form class="form-inline" action="#">
                	<?php if(!isset($_COOKIE['idSession'])){ ?>
        	            <button type="button" id="triggerconnection" class="btn btn-outline-danger mr-2">Se connecter</button>
        	            <button type="button" id= "triggerinscription" class="btn btn-outline-danger">Inscription</button>
        	        <?php } else { ?>
        	        	<a type="button" class="btn btn-outline-danger" href="deconnection.php">Se deconnecter</a>
        	        <?php } ?>
                </form>
            </nav>

            <!-- Modal Connection -->
            <div class="modal fade" id="modalconnection" tabindex="-1" role="dialog">
            	<div class="modal-dialog" role="document">
            		<div class="modal-content">
            	      	<div class="modal-header">
            		        <h5 class="modal-title" id="exampleModalLabel">Connexion</h5>
            			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            			          	<span aria-hidden="true">&times;</span>
            			        </button>
            		    </div>
            		    <div class="modal-body">
                            <?php 
                                if(isset($msgcon)){
                                    echo "<strong style = 'color : red;'> " . $msgcon . "</strong>";
                                }
                            ?>
            		    	<form action="" method="post">
            					<div class="form-group">
            				    	<label for="pseudo">Pseudo:</label>
            				    	<input type="pseudo" class="form-control" placeholder="Entrez votre pseudo" name="pseudo2" id="pseudo2">
            				  	</div>
            				  	<div class="form-group">
            				    	<label for="pwd">Mot de passe:</label>
            				    	<input type="password" class="form-control" placeholder="Entrez votre mot de passe" name="mdp2" id="mdp2">
            				  	</div>
            				  	<button type="submit" id="connectionbtn" name = "formconnection" class="btn btn-primary">Se connecter</button>
            				</form>
            		    </div>
            		    <div class="modal-footer">
            		    	Pas encore de compte ?
            		    	<button type="button" class="btn btn-outline-danger mr-sm-5" data-toggle="modal" data-dismiss="modal" data-target="#modalinscription">Inscription</button>
            		      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            		    </div>
            	    </div>
            	 </div>
			</div>
			
            <!-- Modal Inscription -->
            <div class="modal fade" id="modalinscription" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            	<div class="modal-dialog" role="document">
            		<div class="modal-content">
            	      	<div class="modal-header">
            		        <h5 class="modal-title" id="exampleModalLabel">Inscription</h5>
            			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            			          	<span aria-hidden="true">&times;</span>
            			        </button>
            		    </div>
            		    <div class="modal-body">
            		    	<?php 
            		    		if(isset($msginsc)){
            		    			echo "<strong style = 'color : red;'> " . $msginsc . "</strong>";
            		    		}
            		    	?>
            		    	<form action="" method="post">
            					<div class="form-group">
            				    	<label for="email">Adresse e-mail:</label>
            				    	<input type="email" class="form-control" placeholder="Entrez votre email" name="email1" id="email1">
            				  	</div>
            				  	<div class="form-group">
            				  		<label for="pseudo">Pseudo :</label>
            				  		<input type="name" class="form-control" placeholder="Entrez votre pseudo" name ="pseudo1" id="pseudo1">
            				  	</div>
            				  	<div class="form-group">
            				    	<label for="pwd">Mot de passe:</label>
            				    	<input type="password" class="form-control" placeholder="Entrez votre mot de passe" name="mdp1" id="mdp1">
            				  	</div>
            				  	<button type="submit" name="forminscription" class="btn btn-primary">S'inscrire</button>
            				</form>
            		    </div>
            		    <div class="modal-footer">
            		    	<label for="pwd">Déjà inscrit ? <button type="button" class="btn btn-outline-danger mr-sm-5" data-toggle="modal" data-dismiss="modal" data-target="#modalconnection">Se connecter</button></label>
            		      	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            		    </div>
            	    </div>
            	 </div>
            </div>